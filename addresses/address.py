import requests


class Address:
    def __init__(self, api_key):
        self.apiKey = api_key
        self.url="https://maps.googleapis.com/maps/api/geocode/json"


    def external(self, address):
        url = (self.url + "?address={}&key={}").format(address, self.apiKey)
        print(url)
        return requests.get(url).json()

    def check_position(self, q):
        result = self.external(q)
        print(result)
        lat = result['results'][0]['geometry']['location']['lat']
        lng = result['results'][0]['geometry']['location']['lng']
        return {
            "lat": lat,
            "lng": lng,
        }
