from flask import Flask, escape, request
import os
from zones import zones
from snowbans import snowbans
import addresses.address
from datetime import datetime
from dotenv import load_dotenv
import os

load_dotenv()

GMAPS_API_KEY = os.getenv("GMAPS_API_KEY")

app = Flask(__name__)

@app.route('/address')
def address_route():
    a = addresses.address.Address(GMAPS_API_KEY)
    address = request.args.get('q')
    result = a.check_position(address)

    return result

@app.route('/zones')
def zones_route():
    z = zones.Zones()
    lat = float(request.args.get('lat'))
    lng = float(request.args.get('lng'))
    result = z.check_point(lat, lng)

    return {
        "zone": result
    }

@app.route('/snowbans')
def snow_bans():
    zone_param = request.args.get('zone')
    sb = snowbans.Snowbans()
    return sb.load_zones()[zone_param]

@app.route('/ami')
def ami():
    address = request.args.get('address')
    a = addresses.address.Address(GMAPS_API_KEY)
    z = zones.Zones()
    sb = snowbans.Snowbans()
    point = a.check_position(address)
    zone = z.check_point(float(point['lat']), float(point['lng']))
    last_ban = sb.load_zones()[zone]
    now = datetime.now()
    d1 = now.strftime("%Y-%m-%dT%H:%M:%s")
    return {
        "banned_now": last_ban['shift_start'] <= d1 <= last_ban['shift_end'],
        "position": point,
        "zone": zone,
        "last_ban": last_ban,
    }



if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)