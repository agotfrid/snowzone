import requests


class Snowbans:
    def __init__(self):
        self.last_bans = {}
        self.url = "https://data.winnipeg.ca/resource/tix9-r5tc.json"

    def external(self):
        return requests.get(self.url).json()

    def load_zones(self):
        plow_zone_bans = self.external()
        for row in plow_zone_bans:
            if row['plow_zone'] not in self.last_bans.keys():
                self.last_bans[row['plow_zone']] = {
                    "shift_start": row['shift_start'],
                    "shift_end": row['shift_end'],
                }
            elif self.last_bans[row['plow_zone']]['shift_end'] < row['shift_end']:
                self.last_bans[row['plow_zone']] = {
                    "shift_start": row['shift_start'],
                    "shift_end": row['shift_end'],
                }
        return self.last_bans